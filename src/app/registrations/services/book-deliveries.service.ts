import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { asyncMemoize, DiagnosticsService } from '@universis/common';

@Injectable({
  providedIn: 'root'
})
export class BookDeliveriesService {

  private readonly _bookDeliveriesURL: string = 'students/me/bookDeliveries';
  private readonly _registrationBooksURL: string = 'students/me/registrationBooks';

  constructor(private _context: AngularDataContext,
    private _diagnosticsService: DiagnosticsService) { }


  async supported() {
    try {
      return await this._diagnosticsService.hasService('EudoxusService');
    } catch (err) {
      console.error(err);
      return Promise.resolve(false);
    }
  }


  @asyncMemoize()
  async fetchBookDeliveries(year: number, period: number) {
    try {
      const data = {
        academicYear: year,
        academicPeriod: period
      };
      const bookDeliveriesResponse = await this._context.model(this._bookDeliveriesURL).save(data);
      if (bookDeliveriesResponse && bookDeliveriesResponse.hasOwnProperty('books') === true) {
        return bookDeliveriesResponse.books;
      } else {
        console.log('E_ERROR', 'BOOK_DELIVERIES service response not matches BOOK_DELIVERIES specifications')
        return null;
      }
    } catch (err) {
      console.error(err);
      return Promise.resolve(null);
    }
  }

  @asyncMemoize()
  async fetchRegistrationBooks(year: number, period: number) {
    try {
      const data = {
        academicYear: year,
        academicPeriod: period
      };
      const bookDeliveriesResponse = await this._context.model(this._registrationBooksURL).save(data);
      if (bookDeliveriesResponse) {
        return bookDeliveriesResponse;
      } else {
        return null;
      }
    } catch (err) {
      // return book service error 
        return Promise.resolve({
          bookServiceError: true
        });
    }
  }


}
